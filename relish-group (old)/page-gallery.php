<?php
/*
Template Name: Gallery Page
*/
get_header(); ?>
	<?php

	$args = array(
	   'post_type' => 'attachment',
	   'numberposts' => -1,
	   'post_status' => null,
	   'post_parent' => $post->ID,
	   'order'=> 'ASC',
	 );

	$attachments = get_posts( $args );

	// $args = array(
	// 	'numberposts' => -1, // Using -1 loads all posts
	// 	'orderby' => 'menu_order', // This ensures images are in the order set in the page media manager
	// 	'order'=> 'ASC',
	// 	'post_mime_type' => 'image', // Make sure it doesn't pull other resources, like videos
	// 	'post_parent' => $post->ID, // Important part - ensures the associated images are loaded
	// 	'post_status' => null,
	// 	'post_type' => 'attachment'
	// );

	// $images = get_children( $args );
	// continued below ...

	// continued from above ...
	if($images){ ?>
	<!--div id="slider">
		<?php foreach($images as $image){ ?>
		<img src="<?php echo $image->guid; ?>" alt="<?php echo $image->post_title; ?>" title="<?php echo $image->post_title; ?>" />
		<?php	} ?>
	</div-->
  	<?php } ?>

<?php 

$szPostContent = $post->post_content;
$szSearchPattern = '~<img [^\>]*\ />~';

// Run preg_match_all to grab all the images and save the results in $aPics
preg_match_all( $szSearchPattern, $szPostContent, $aPics );

// Check to see if we have at least 1 image
$iNumberOfPics = count($aPics[0]);


$pagekids = get_pages("child_of=".$post->ID."&sort_column=menu_order");
$page0 = $pagekids[0];
$page1 = $pagekids[1];
$page2 = $pagekids[2];

?>
	<div id="mainContainer">
		<div class="leftContent-<?php echo $post->ID; ?>">

			<div class="restCont">
				<div class="headerR">
					<img src="<?php bloginfo('template_directory'); ?>/images/a<?php echo $post->ID; ?>.png" alt="" title="" />
				</div>
				<script type="text/javascript">
					(function( $ ){
						$(document).ready(function(){	
							$(".Rhover").click(function(){
								$(".Rhover").removeClass('Selector');
								$(this).addClass('Selector');
							});
						});
					})(jQuery);

				</script>


				<div class="headerNav">
					<ul>
						<li class="Rhover" onclick="getSubContent(<?php echo $page0->ID; ?>)">About</li>
						<li class="separator">/</li>
						<li class="Rhover" onclick="getSubContent(<?php echo $page1->ID; ?>)">Menu</li>
						<li class="separator">/</li>
						<li class="Rhover" onclick="getSubContent(<?php echo $page2->ID; ?>)">Locations</li>
					</ul>
				</div>
				<div class="contentSub">
					
				</div>

				<div class="likeButtons">
					follow <?php echo $post->post_title; ?>
					<?php
						if ($post->ID == "9"){
							$FBlink = "https://www.facebook.com/WeeNamKeePH";
							$twitterlink = "https://twitter.com/WeeNamKeePH";
							echo'
							<style>
img.fill {
  position: relative;
  left: -38px;
}
</style>';
						} else if ($post->ID == "10"){
							$FBlink = "https://www.facebook.com/OsakaOhshoPH";
							$twitterlink = "https://www.twitter.com/OsakaOhshoPH";
							echo'
							<style>
img.fill {
  position: relative;
  left: -38px;
}
</style>';
						} else if ($post->ID == "261"){
							$FBlink = "https://www.facebook.com/kumoriph";
							$instalink = "https://instagram.com/kumoriph";
							echo'
							<style>
.twitter {
 display: none;
}
</style>';
						} else {
							$FBlink = "#";
							$twitterlink = "#";
						}
					?> 
					<a href="<?php echo $FBlink; ?>" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/fb.png"></a>
					<a href="<?php echo $twitterlink; ?>" target="_blank" class="twitter"><img src="<?php bloginfo('template_directory'); ?>/images/tw.png"></a>
                    
                    <a href="<?php echo $instalink; ?>" target="_blank" class="insta"><img src="<?php bloginfo('template_directory'); ?>/images/ig.png"></a>
                <img class="fill" src="<?php bloginfo('template_directory'); ?>/images/fill.gif" width="30" height="29" /> </div>
			</div>
		</div>
		<div class="rightContent">
            <ul id="m_slide_holder2">


                <?php 
					if ( $iNumberOfPics > 0 ) {
					     // Now here you would do whatever you need to do with the images
					     // For this example the images are just displayed
					     for ( $i=0; $i < $iNumberOfPics ; $i++ ) {
					          echo '<li>'.$aPics[0][$i].'</li>';
					     };
					};

                ?>
            </ul>
            <div class="floatingC">
				<div class="navGal">
					<div class="navL"></div>
					<div class="navCount"></div>
					<div class="navR"></div>
				</div>
				<div class="imageName" id="m_photo_description">


               		 <?php 
						if ( $iNumberOfPics > 0 ) {
						     // Now here you would do whatever you need to do with the images
						     // For this example the images are just displayed
						     for ( $i=0; $i < $iNumberOfPics ; $i++ ) {
						     	preg_match_all('/(alt)=("[^"]*")/i',$aPics[0][$i], $alt);
						        echo '<div>'.str_replace('"', "", $alt[2][0]).'</div>';
						     };
						};

	                ?>
				</div>
				<div class="navF">
					<span class="currentSlide"></span> of <span class="totalSlide"></div>
				</div>
            </div>
		</div>
	</div>


<script type="text/javascript">

function showResponse(originalRequest){
	jQuery('.contentSub').html(originalRequest);
}

function getSubContent(id){
	jQuery.ajax( {type: "POST", url: '<?php echo get_template_directory_uri(); ?>/getSubText.php', data: "id="+ id, success: showResponse});
}	

jQuery(document).ready(function(){
	getSubContent(<?php echo $page0->ID; ?>);
});


</script>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>