<?php
/*
Template Name: Mailing List Page
*/

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">
			<article id="post-109" class="post-109 page type-page status-publish hentry">

				<header class="entry-header">
					<h1 class="entry-title">Join Our Mailing List</h1>
				</header>
				<div class="entry-content">
					<div id="joinForm">
						<form action="" onsubmit="return subscribeMailList()">
							<input type="text" id="ml-email" autocomplete="off" value="email address" onfocus="deleteMe(this);" style="color: #1097FF !important;">
							<input type="submit" value="Submit">
						</form>
					</div>
					<div id="successForm" style="display: none;">
						Check your email and confirm the subscription
					</div>
				</div>
			</article>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>

