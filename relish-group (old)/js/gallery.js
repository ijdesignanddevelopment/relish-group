
(function( $ ){
	$(document).ready(function(){	
					
		//for the modal box
		if($('#m_slide_holder2').children().length){
			var previous_index = 0;
			var total_images = $('#m_slide_holder2 li').length;
			var thumbs_per_page = 5; //the number of thumbnails shown
			var slide_size = 600; //use to animate the thumbnail
			var total_thumb_sets = Math.ceil(total_images/thumbs_per_page);
			var clicked = false;
			
			$('.totalSlide').text(total_images); 

			$('#m_slide_holder2').cycle({ 
				fx:     'scrollLeft', 
				speed:  '1500', 
				timeout: 10, 
				next:   '.navR', 
				prev:   '.navL',
				pager:  '.imageNav ul', 
				activePagerClass: 'sel', // class name used for the active pager element 
				pagerAnchorBuilder: function(idx, slide) { 
					//generates the thumbnail
					return '<li></li>'; 
				},
				after: function(current, next, options){
					current_index = $(this).index();
					var rT = total_images - 1;

					if(current_index==rT){

						console.log('last');
					} else {


					}


					thumb_set = 0;
					
					for(i = thumbs_per_page;i <= (total_thumb_sets * thumbs_per_page); i+=thumbs_per_page){
						if(current_index < i){
							break;
						}
						
						thumb_set++;
					}
					
					required_right_value = thumb_set * slide_size;
					
					//$('#m_thumbnails ul').animate({'right':required_right_value});
					
					/*
						add dn class to all child elements of #g_image_description
						
						.dn{
							display: none !important;
						}
					*/
					$('#m_photo_description').children().addClass('dn'); 
					
					//remove the dn class from the current description

					$('#m_photo_description div:eq('+current_index+')').removeClass('dn');
					
					//change the current page number of the element
					$('.navCount').text(current_index + 1); 
					$('.currentSlide').text(current_index + 1); 

					// for refreshing the ads

					//googletag.pubads().refresh([slot1, slot2]);

					return false;
				} 
			});


		}
	});


})(jQuery);
