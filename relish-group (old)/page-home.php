<?php
/*
Template Name: Home Page
*/
get_header(); ?>
	<?php

	$args = array(
		'numberposts' => -1, // Using -1 loads all posts
		'orderby' => 'menu_order', // This ensures images are in the order set in the page media manager
		'order'=> 'ASC',
		'post_mime_type' => 'image', // Make sure it doesn't pull other resources, like videos
		'post_parent' => $post->ID, // Important part - ensures the associated images are loaded
		'post_status' => null,
		'post_type' => 'attachment'
	);

	$images = get_children( $args );
	// continued below ...

	// continued from above ...
	?>

	<div id="mainContainer">
<div class="hpimage"><a href="http://relish-group.com/?page_id=10"><img src="http://relish-group.com/wp-content/uploads/2015/03/hp_01.jpg" width="322" height="465" /></a></div>
<div class="hpimage"><a href="http://relish-group.com/?page_id=10"><img src="http://relish-group.com/wp-content/uploads/2015/03/hp_02.jpg" width="321" height="465" /></a></div>
<div class="hpimage"><a href="http://relish-group.com/?page_id=261"><img src="http://relish-group.com/wp-content/uploads/2015/03/hp_03.jpg" width="322" height="465" /></a></div>
	</div>


<?php //get_sidebar(); ?>
<?php get_footer(); ?>