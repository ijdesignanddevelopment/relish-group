<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle.all.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/gallery.js?v=2" type="text/javascript"></script>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
</head>

<body <?php body_class(); ?> id="bodyBG">
<script>
  jQuery(function() {
    jQuery( "#birthday" ).datepicker();
    jQuery( "#visit" ).datepicker();
  });
</script>
<script type="text/javascript">

	function success_add(){
		jQuery('.systemMessage').html('Thank you for sharing your feedback. <br /> <br /><input type="button" name="close" id="formBtn" value="Close" onclick="jQuery(\'#shareFeed\').hide();">');
		jQuery('#frmShareFeedback').hide();
	}

	function error_add(errs){
		alert(errs);
	}


	function success_add2(){
		jQuery('.systemMessage2').html('Check your email and confirm the subscription. <br /> <br /><input type="button" name="close" id="formBtn" value="Close" onclick="jQuery(\'#mailingList\').hide();">');
		jQuery('#frmMailingList').hide();
	}

	function closePop(id){
		jQuery('#'+id).hide();
	}


	function selectbranch(){
		if(jQuery('#restaurant').val()=="Wee Nam Kee"){
			jQuery('#wnk_res').show();
			jQuery('#oo_res').hide();
		} else if(jQuery('#restaurant').val()=="Osaka Ohsho"){
			jQuery('#wnk_res').hide();
			jQuery('#oo_res').show();
		} 
	}

	function firstF(){
		if(jQuery('#coupon_code').val()!=""){
			jQuery('.form1').hide(); 
			jQuery('.form2').show();
		} else {
			alert("Coupon Code is required!");
		}
	}

	function secF(){
		var a = 0;

		if(jQuery('#named').val()==""){
			a++;
		}

		if(jQuery('#email').val()==""){
			a++;
		}

		if(jQuery('#company').val()==""){
			a++;
		}

		if(jQuery('#address').val()==""){
			a++;
		}

		if(jQuery('#mobile').val()==""){
			a++;
		}

		if(jQuery('#bdayDay').val()==""){
			a++;
		}

		if(jQuery('#bdayMo').val()==""){
			a++;
		}

		if(jQuery('#bdayYear').val()==""){
			a++;
		}

		//if(jQuery('#aniversary').val()==""){
		//	a++;
		//}

		if(jQuery('#restaurant').val()==""){
			a++;
		}

		if(jQuery('#visitDay').val()==""){
			a++;
		}

		if(jQuery('#visitMo').val()==""){
			a++;
		}

		if(jQuery('#visitYear').val()==""){
			a++;
		}
		if(a>0){
			alert("All fields are required.");
		} else {
			jQuery('.form2').hide();
			jQuery('.form3').show();
		}
	}

	function thirdF(){
		var a = 0;
		if(jQuery('#most').val()==""){
			a++;
		}

		if(jQuery('#why1').val()==""){
			a++;
		}

		if(jQuery('#least').val()==""){
			a++;
		}

		if(jQuery('#why2').val()==""){
			a++;
		}

		var checkbox1 = jQuery("input[name=r1]:checked");
		var checkbox2 = jQuery("input[name=r2]:checked");
		var checkbox3 = jQuery("input[name=r3]:checked");
		var checkbox4 = jQuery("input[name=r4]:checked");
		var checkbox5 = jQuery("input[name=r5]:checked");
		var checkbox6 = jQuery("input[name=r6]:checked");
		var checkbox7 = jQuery("input[name=r7]:checked");
		var checkbox8 = jQuery("input[name=r8]:checked");

		if( checkbox1.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox2.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox3.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox4.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox5.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox6.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox7.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox8.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		console.log(a);

		if(a>0){
			alert("All fields are required.");
		} else {
			jQuery('.form3').hide(); 
			jQuery('.form4').show();
		}
	}


	function fortF(){
		var a = 0;
		if(jQuery('#server').val()==""){
			a++;
		}

		var checkbox1 = jQuery("input[name=r9]:checked");
		var checkbox2 = jQuery("input[name=r10]:checked");
		var checkbox3 = jQuery("input[name=r11]:checked");
		var checkbox4 = jQuery("input[name=r12]:checked");
		var checkbox5 = jQuery("input[name=r13]:checked");
		var checkbox6 = jQuery("input[name=r14]:checked");

		if( checkbox1.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox2.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox3.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox4.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox5.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox6.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}


		console.log(a);

		if(a>0){
			alert("All fields are required.");
		} else {
			jQuery('.form4').hide(); 
			jQuery('.form5').show();
		}

		
	}

	function fifthF(){

		var a = 0;

		var checkbox1 = jQuery("input[name=r15]:checked");
		var checkbox2 = jQuery("input[name=r16]:checked");
		var checkbox3 = jQuery("input[name=r17]:checked");
		var checkbox4 = jQuery("input[name=r18]:checked");
		var checkbox5 = jQuery("input[name=r19]:checked");

		if( checkbox1.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox2.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox3.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox4.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		if( checkbox5.length > 0 ) {
			//alert( checkbox.val() ); // checkbox value
		} else {
		   	a++;
		}

		console.log(a);

		if(a>0){
			alert("All fields are required.");
		} else {
			jQuery('.form5').hide(); 
			jQuery('.form6').show();
		}

	}

	function sixF(vcode){
		jQuery('#vcode').val(vcode);
		jQuery('.form6').hide(); 
		jQuery('.form7').show();
	}

	function downloads(){
		window.open('http://relish-group.com/Voucher/generate/relish.php?vcode='+jQuery('#vcode').val(),'Voucher','toolbar=0,status=0,width=800,height=600');

		jQuery(':input','#frmShareFeedback')
		  .not(':button, :submit, :reset, :hidden')
		  .val('')
		  .removeAttr('checked')
		  .removeAttr('selected');

		jQuery('.form7').hide(); 
		jQuery('.form1').show();
		closePop('shareFeed');
	}



</script>
<div id="shareFeed" class="overlay" style="display: none;">
	<div class="overlayBox">
		<div class="closeBtn"  onclick="closePop('shareFeed')"></div>
		<div class="olHeader">SHARE FEEDBACK</div>
		<div class="systemMessage"></div>
		<form name="frmShareFeedback" id="frmShareFeedback" action="http://relish-group.com/Gdata/save.php" method="post"  target="atrFrame" >
		
		<div class="form1">
			<table>
				<tr>
					<td colspan="2">We at the Relish Group take pride in providing you with the highest standards of QUALITY, SERVICE, CLEANLINESS and VALUE in the restaurant industry. Your opinion is extremely important in evaluating our business and we hope you could take some time to complete this survey.  To show our appreciation, please expect a coupon for a free dish at the end of the survey.</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">Please enter the coupon code found on your receipt:</td>
				</tr>
				<tr>
					<td colspan="2"><input type="text" name="coupon_code" id="coupon_code" class="formText" /></td>
				</tr>
				<tr>
					<td></td>
					<td style="text-align: right;">
						<input type="button" name="next" id="formBtn" value="next" onclick="firstF()">
					</td>
				</tr>
			</table>
		</div>
		<div class="form2"  style="display: none;">
			<table>
				<tr>
					<td width="90px">Name</td>
					<td><input type="text" name="named" id="named" class="formText" /></td>
				</tr>
				<tr>
					<td>Email Ad</td>
					<td><input type="text" name="email"  id="email" class="formText" /></td>
				</tr>
				<tr>
					<td>Company</td> 
					<td><input type="text" name="company"  id="company" class="formText" /></td>
				</tr>
				<tr>
					<td>Address</td>
					<td><input type="text" name="address" id="address" class="formText" /></td>
				</tr>
				<tr>
					<td>Mobile<br>Number</td>
					<td><input type="text" name="mobile" id="mobile" class="formText" /></td>
				</tr>
				<tr>
					<td>Birthday</td>
					<td>
						<select name="bdayDay" id="bdayDay">
							<?php
								$options = "";
								for($i=1;$i<=31;$i++){
									$value = substr($i+100,1);
									$name = substr($i+100,1);
									$options .= '<option value="'.$value.'" '; 
									if($value == $selected){ $options .='selected="selected"';}
									$options .='>'.$name.'</option>';
								}
								echo $options;
							?>
						</select>
						<select name="bdayMo" id="bdayMo">
							<?php
								$options = "";
								$monthlist = array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
								for($i=1;$i<=12;$i++){
									$value = substr($i+100,1);
									$name = $monthlist[$i];
									$options .= '<option value="'.$value.'" '; 
									if($value == $selected){ $options .='selected="selected"';}
									$options .='>'.$name.'</option>';
								}
								echo $options;
							?>
						</select>
						<select name="bdayYear" id="bdayYear">
							<?php
								$options = "";
								$yr = date("Y");
								for($i=$yr;$i>=1950;$i--){			
									$value = $i;
									$name = $i;
									$options .= '<option value="'.$value.'" '; 
									if($value == $selected){ $options .='selected="selected"';}
									$options .='>'.$name.'</option>';
								}
								echo $options;
							?>
						</select>
					</td>
				</tr>
				<!-- <tr>
					<td>Aniversary</td>
					<td><input type="text" name="aniversary" id="aniversary" class="formText" /></td>
				</tr> -->
				<tr>
					<td>Restaurant Visited</td>
					<td>
						<select name="restaurant" id="restaurant"  id="restaurant" onchange="selectbranch();">
							<option value="">Select Restaurant</option>
							<option value="Wee Nam Kee">Wee Nam Kee</option>
							<option value="Osaka Ohsho">Osaka Ohsho</option>
						</select>

						<select name="wnk_res" id="wnk_res" style="display: none;">
							<option value="">Select Branch</option>
							<option value="Alabang Town Center">Alabang Town Center</option>
							<option value="Ayala Triangle Gardens">Ayala Triangle Gardens</option>
							<option value="The District Imus">The District Imus</option>
							<option value="Fairview Terraces">Fairview Terraces</option>
							<option value="Glorietta 2">Glorietta 2</option>
							<option value="Promenade 3">Promenade 3</option>
							<option value="Serendra">Serendra</option>
							<option value="Shangri-La Plaza">Shangri-La Plaza</option>
							<option value="TriNoma">TriNoma</option>
						</select>

						<select name="oo_res" id="oo_res" style="display: none;">
							<option value="">Select Branch</option>
							<option value="Megamall">Megamall</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Date of Visit</td>
					<td>
						<select name="visitDay" id="visitDay">
							<?php
								$options = "";
								for($i=1;$i<=31;$i++){
									$value = substr($i+100,1);
									$name = substr($i+100,1);
									$options .= '<option value="'.$value.'" '; 
									if($value == $selected){ $options .='selected="selected"';}
									$options .='>'.$name.'</option>';
								}
								echo $options;
							?>
						</select>
						<select name="visitMo" id="visitMo">
							<?php
								$options = "";
								$monthlist = array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
								for($i=1;$i<=12;$i++){
									$value = substr($i+100,1);
									$name = $monthlist[$i];
									$options .= '<option value="'.$value.'" '; 
									if($value == $selected){ $options .='selected="selected"';}
									$options .='>'.$name.'</option>';
								}
								echo $options;
							?>
						</select>
						<select name="visitYear" id="visitYear">
							<?php
								$options = "";
								$yr = date("Y");
								for($i=$yr;$i>=1950;$i--){			
									$value = $i;
									$name = $i;
									$options .= '<option value="'.$value.'" '; 
									if($value == $selected){ $options .='selected="selected"';}
									$options .='>'.$name.'</option>';
								}
								echo $options;
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;" ><!--input type="button" name="next" id="formBtn" value="back"  onclick="jQuery('.form2').hide(); jQuery('.form1').show();"--></td>
					<td style="text-align: right;" colspan="5"><input type="button" name="next" id="formBtn" value="next"  onclick="secF();" ></td>
				</tr>
			</table>
		</div>

		<div class="form3" style="display: none;">
			<table>
				<tr>
					<td class="headr" colspan="6">Instructions: Please evaluate your dining experience by providing a score from 1 to 5, with 5 being the highest and 1 being the lowest. If an item is not applicable, simply mark it as N/A.</td>
				</tr>
				<tr>
					<td class="headr" colspan="6">&nbsp;</td>
				</tr>
				<tr>
					<td class="headr" colspan="6"><strong>Food and Beverage</strong></td>
				</tr>
				<tr>
					<td class="feedbackF">Which dishes did you like the most?</td>
					<td colspan="5"><input type="text" name="most" id="most" class="formTextS" /></td>
				</tr>
				<tr>
					<td class="feedbackF">Why?</td>
					<td colspan="5"><input type="text" name="why1" id="why1" class="formTextS" /></td>
				</tr>
				<tr>
					<td class="feedbackF">Which dishes did you like the least?</td>
					<td colspan="5"><input type="text" name="least" id="least" class="formTextS" /></td>
				</tr>
				<tr>
					<td class="feedbackF">Why?</td>
					<td colspan="5"><input type="text" name="why2" id="why2" class="formTextS" /></td>
				</tr>
				<tr>
					<td class="feedbackF">Quality and Taste of the Beverages</td>
					<td><input type="radio" name="r1" value="5">5</td>
					<td><input type="radio" name="r1" value="4">4</td>
					<td><input type="radio" name="r1" value="3">3</td>
					<td><input type="radio" name="r1" value="2">2</td>
					<td><input type="radio" name="r1" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Quality and Taste of the Food</td>
					<td><input type="radio" name="r2" value="5">5</td>
					<td><input type="radio" name="r2" value="4">4</td>
					<td><input type="radio" name="r2" value="3">3</td>
					<td><input type="radio" name="r2" value="2">2</td>
					<td><input type="radio" name="r2" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Presentation of the Food</td>
					<td><input type="radio" name="r3" value="5">5</td>
					<td><input type="radio" name="r3" value="4">4</td>
					<td><input type="radio" name="r3" value="3">3</td>
					<td><input type="radio" name="r3" value="2">2</td>
					<td><input type="radio" name="r3" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Presentation of the Beverages</td>
					<td><input type="radio" name="r4" value="5">5</td>
					<td><input type="radio" name="r4" value="4">4</td>
					<td><input type="radio" name="r4" value="3">3</td>
					<td><input type="radio" name="r4" value="2">2</td>
					<td><input type="radio" name="r4" value="1">1</td>
				</tr>				
				<tr>
					<td class="feedbackF">Temperature of the Food</td>
					<td><input type="radio" name="r5" value="5">5</td>
					<td><input type="radio" name="r5" value="4">4</td>
					<td><input type="radio" name="r5" value="3">3</td>
					<td><input type="radio" name="r5" value="2">2</td>
					<td><input type="radio" name="r5" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Portion Size</td>
					<td><input type="radio" name="r6" value="5">5</td>
					<td><input type="radio" name="r6" value="4">4</td>
					<td><input type="radio" name="r6" value="3">3</td>
					<td><input type="radio" name="r6" value="2">2</td>
					<td><input type="radio" name="r6" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Variety of Menu Items </td>
					<td><input type="radio" name="r7" value="5">5</td>
					<td><input type="radio" name="r7" value="4">4</td>
					<td><input type="radio" name="r7" value="3">3</td>
					<td><input type="radio" name="r7" value="2">2</td>
					<td><input type="radio" name="r7" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Value for Money</td>
					<td><input type="radio" name="r8" value="5">5</td>
					<td><input type="radio" name="r8" value="4">4</td>
					<td><input type="radio" name="r8" value="3">3</td>
					<td><input type="radio" name="r8" value="2">2</td>
					<td><input type="radio" name="r8" value="1">1</td>
				</tr>
				<tr>
					<td style="text-align: left;" ><!--input type="button" name="next" id="formBtn" value="back"  onclick="jQuery('.form3').hide(); jQuery('.form2').show();"--></td>
					<td style="text-align: right;" colspan="5"><input type="button" name="next" id="formBtn" value="next"  onclick="thirdF();" ></td>
				</tr>
			</table>
		</div>

		<div class="form4" style="display: none;">
			<table>
				<tr>
					<td class="headr" colspan="6"><strong>Service</strong></td>
				</tr>
				<tr>
					<td class="feedbackF">Server Name</td>
					<td colspan="5"><input type="text" name="server" id="server" class="formTextS" /></td>
				</tr>
				<tr>
					<td class="feedbackF">Timely Service</td>
					<td><input type="radio" name="r9" value="5">5</td>
					<td><input type="radio" name="r9" value="4">4</td>
					<td><input type="radio" name="r9" value="3">3</td>
					<td><input type="radio" name="r9" value="2">2</td>
					<td><input type="radio" name="r9" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Server Attentiveness</td>
					<td><input type="radio" name="r10" value="5">5</td>
					<td><input type="radio" name="r10" value="4">4</td>
					<td><input type="radio" name="r10" value="3">3</td>
					<td><input type="radio" name="r10" value="2">2</td>
					<td><input type="radio" name="r10" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Knowledgeability of the Server</td>
					<td><input type="radio" name="r11" value="5">5</td>
					<td><input type="radio" name="r11" value="4">4</td>
					<td><input type="radio" name="r11" value="3">3</td>
					<td><input type="radio" name="r11" value="2">2</td>
					<td><input type="radio" name="r11" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Server Grooming and Appearance</td>
					<td><input type="radio" name="r12" value="5">5</td>
					<td><input type="radio" name="r12" value="4">4</td>
					<td><input type="radio" name="r12" value="3">3</td>
					<td><input type="radio" name="r12" value="2">2</td>
					<td><input type="radio" name="r12" value="1">1</td>
				</tr>				
				<tr>
					<td class="feedbackF">Friendliness of the Server </td>
					<td><input type="radio" name="r13" value="5">5</td>
					<td><input type="radio" name="r13" value="4">4</td>
					<td><input type="radio" name="r13" value="3">3</td>
					<td><input type="radio" name="r13" value="2">2</td>
					<td><input type="radio" name="r13" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Availability of Aauces, Utensils, Napkins, etc.</td>
					<td><input type="radio" name="r14" value="5">5</td>
					<td><input type="radio" name="r14" value="4">4</td>
					<td><input type="radio" name="r14" value="3">3</td>
					<td><input type="radio" name="r14" value="2">2</td>
					<td><input type="radio" name="r14" value="1">1</td>
				</tr>
				<tr>
					<td style="text-align: left;" ><!--input type="button" name="next" id="formBtn" value="back"  onclick="jQuery('.form4').hide(); jQuery('.form3').show();"--></td>
					<td style="text-align: right;" colspan="5"><input type="button" name="next" id="formBtn" value="next"  onclick="fortF(); " ></td>
				</tr>
			</table>
		</div>

		<div class="form5" style="display: none;">
			<table style="width: 405px;">
				<tr>
					<td class="headr" colspan="6"><strong>Others</strong></td>
				</tr>
				<tr>
					<td class="feedbackF">Overall Ambience</td>
					<td><input type="radio" name="r15" value="5">5</td>
					<td><input type="radio" name="r15" value="4">4</td>
					<td><input type="radio" name="r15" value="3">3</td>
					<td><input type="radio" name="r15" value="2">2</td>
					<td><input type="radio" name="r15" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Lighting</td>
					<td><input type="radio" name="r16" value="5">5</td>
					<td><input type="radio" name="r16" value="4">4</td>
					<td><input type="radio" name="r16" value="3">3</td>
					<td><input type="radio" name="r16" value="2">2</td>
					<td><input type="radio" name="r16" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Background Music</td>
					<td><input type="radio" name="r17" value="5">5</td>
					<td><input type="radio" name="r17" value="4">4</td>
					<td><input type="radio" name="r17" value="3">3</td>
					<td><input type="radio" name="r17" value="2">2</td>
					<td><input type="radio" name="r17" value="1">1</td>
				</tr>
				<tr>
					<td class="feedbackF">Dining Acoustics </td>
					<td><input type="radio" name="r18" value="5">5</td>
					<td><input type="radio" name="r18" value="4">4</td>
					<td><input type="radio" name="r18" value="3">3</td>
					<td><input type="radio" name="r18" value="2">2</td>
					<td><input type="radio" name="r18" value="1">1</td>
				</tr>				
				<tr>
					<td class="feedbackF">Cleanliness</td>
					<td><input type="radio" name="r19" value="5">5</td>
					<td><input type="radio" name="r19" value="4">4</td>
					<td><input type="radio" name="r19" value="3">3</td>
					<td><input type="radio" name="r19" value="2">2</td>
					<td><input type="radio" name="r19" value="1">1</td>
				</tr>
				<tr>
					<td style="text-align: left;" ><!--input type="button" name="next" id="formBtn" value="back"  onclick="jQuery('.form5').hide(); jQuery('.form4').show();"--></td>
					<td style="text-align: right;" colspan="5"><input type="button" name="next" id="formBtn" value="next"  onclick="fifthF();" ></td>
				</tr>
			</table>
		</div>

		<div class="form6"  style="display: none;" >
			<table>
				<tr>
					<td class="headr" colspan="6"><strong>Others</strong></td>
				</tr>
				<tr>
					<td colspan="2">
						What else can we do to improve your dinning experience?<br>
						Please share your thoughts with us.
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<textarea name="thoughts" id="thoughts"></textarea>
					</td>
				</tr>

				<tr>
					<td style="text-align: left;" ><!--input type="button" name="next" id="formBtn" value="back"  onclick="jQuery('.form6').hide(); jQuery('.form5').show();"--></td>
					<td style="text-align: right;"><input type="submit" name="submitted" id="formBtn" value="submit"><!--input type="button" name="next" id="formBtn" value="next"  onclick="sixF();" --></td>
				</tr>
			</table>

		</div>

		<div class="form7"  style="display: none;" >
			<table>
				<tr>
					<td class="headr" colspan="6">Thank you for taking a moment in answering our survey.   Please download your free coupon here.<br><br><br>
					See you soon at a Relish Group restaurant!<br><br><br>
					<input type="hidden" name="vcode" id="vcode" value="" /></td>
				</tr>
			

				<tr>
					<td style="text-align: left;" ><!-- <input type="button" name="next" id="formBtn" value="back"  onclick="jQuery('.form7').hide(); jQuery('.form6').show();"> --></td>
					<td style="text-align: right;"><input type="button" name="download" id="formBtn" value="Download Voucher" onclick="downloads();"></td>
				</tr>
			</table>
		</div>

		<iframe width="0" height="0" name="atrFrame" id="atrFrame" src="" scrolling="no" frameborder="0" style="visibility: hidden;"></iframe>
	</form>
	</div>
</div>

<div id="mailingList" class="overlay" style="display: none;">
	<div class="overlayBox">
		<div class="closeBtn" onClick="closePop('mailingList')"></div>
		<div class="olHeader">JOIN OUR MAILING LIST!</div>
		<div class="systemMessage2"></div>
		<form name="frmMailingList" id="frmMailingList" action="http://relish-group.com/wordpress/Gdata/mailinglist.php" method="post"  target="atrFrame2" >
			<div class="form1">
				<table>
					<tr>
						<td width="90px">First Name</td>
						<td><input type="text" name="fname" class="formText" /></td>
					</tr>
					<tr>
						<td width="90px">Last Name</td>
						<td><input type="text" name="lname" class="formText" /></td>
					</tr>
					<tr>
						<td>Birthday</td>
						<td><input type="text" name="month" class="formText2" /> <input type="text" name="day" class="formText2" /> <input type="text" name="year" class="formText2" /></td>
					</tr>
					<tr>
						<td>Email Ad</td>
						<td><input type="text" name="email" class="formText" /></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center;"><input type="submit" name="next" id="joinBtn" value="JOIN"></td>
					</tr>
				</table>
			</div>			
			<iframe width="0" height="0" name="atrFrame2" id="atrFrame2" src="" scrolling="no" frameborder="0" style="visibility: hidden;"></iframe>
		</form>

	</div>
</div>
<div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">
		<hgroup>

			<div class="sfeedback">
				<a href="#" onClick="jQuery('#shareFeed').show();" >Share Feedback</a>
			</div>
			
			<div class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div>
			<div class="mailinglist">
				<a href="#"  onclick="jQuery('#mailingList').show();"  class="mch">Click Here</a>
				<div class="clearfix"></div>
				<!--a href="" class="fbL">Facebook</a>
				<a href="" class="twitL">Twitter</a-->
			</div>
		</hgroup>

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<h3 class="menu-toggle"><?php _e( 'Menu', 'twentytwelve' ); ?></h3>
			<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentytwelve' ); ?>"><?php _e( 'Skip to content', 'twentytwelve' ); ?></a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
		</nav><!-- #site-navigation -->

		<?php $header_image = get_header_image();
		if ( ! empty( $header_image ) ) : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $header_image ); ?>" class="header-image" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>
		<?php endif; ?>
	</header><!-- #masthead -->

	<div id="main" class="wrapper">