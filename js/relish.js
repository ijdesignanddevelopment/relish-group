var overlay = document.createElement("div");
overlay.setAttribute("id","overlay");
overlay.setAttribute("style","display:none;position:fixed;top:0;left:0;background-color:rgba(0, 0, 0, 0.5);z-index:1000;width:100vw;height:100%;");
var body = document.getElementsByTagName("body");
body[0].appendChild(overlay);

var contactForm = document.getElementById("contactForm");
var careerForm = document.getElementById("careerForm");
var fullName = document.getElementById("fullname");
var fullName2 = document.getElementById("fullname2");
var email = document.getElementById("email");
var email2 = document.getElementById("email2");
var message = document.getElementById("message");
var message2 = document.getElementById("message2");

var submit = document.getElementById("submit");
var submit2 = document.getElementById("submit2");

contactForm.addEventListener('submit', function(ev) {
	ev.preventDefault();
	var currLoc = window.location.href;
	var url;
	if ( currLoc.indexOf('http://') > -1 ){
		url = "http://www.relish-group.com/mailhandler.php";
	} else {
		url = "www.relish-group.com/mailhandler.php";
	}
	helper.sendForm(submit2, fullName2, email2, message2, url)
	.then(function(result){
		var parent = document.getElementById("contact");
		var message = document.createElement("p");
		message.classList.add("formSuccess");
		message.innerHTML = "Message submitted!";
		parent.appendChild(message);
		contactForm.reset();
	})
	.catch(function(error){
		var parent = document.getElementById("contact");
		var message = document.createElement("p");
		message.classList.add("formError");
		message.innerHTML = "The server seems to have a problem. Please try again later.";
		parent.appendChild(message);
	});

}, false);

careerForm.addEventListener('submit', function(ev) {
	ev.preventDefault();
	var currLoc = window.location.href;
	var url;
	if ( currLoc.indexOf('http://') > -1 ){
		url = "http://www.relish-group.com/mailhandler.php";
	} else {
		url = "www.relish-group.com/mailhandler.php";
	}
	helper.sendForm(submit, fullName, email, message, url)
	.then(function(result){
		var parent = document.getElementById("careers");
		var message = document.createElement("p");
		message.classList.add("formSuccess");
		message.innerHTML = "Application submitted!";
		parent.appendChild(message);
		careerForm.reset();
	})
	.catch(function(error){
		var parent = document.getElementById("careers");
		var message = document.createElement("p");
		message.classList.add("formError");
		message.innerHTML = "The server seems to have a problem. Please try again later.";
		parent.appendChild(message);
	});

}, false);


// TOGGLE POPOVERS
$(".brandsOn").click(function () {
    $("#brand-bar").toggle();
    	$("#careers").css("display", "none");
    	$("#contact").css("display", "none");
      $("#overlay").css("display","block");
});
$("#careersPull").click(function () {
	$("#careers").toggle();
    	$("#brand-bar").css("display", "none");
    	$("#contact").css("display", "none");
      $("#overlay").css("display","block");
});
$("#contactPull").click(function () {
	$("#contact").toggle();
    	$("#careers").css("display", "none");
    	$("#brand-bar").css("display", "none");
      $("#overlay").css("display","block");
});
$("#overlay").click(function(){
  $("#careers").css("display","none");
  $("#brand-bar").css("display", "none");
  $("#contact").css("display", "none");
  $(this).css("display","none");
});
$(function() {
    var pull        = $('#pull');
        menu        = $('nav ul');
        menuHeight  = menu.height();

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
        var brandBar = $("#brand-bar");
        if(brandBar.css("display") == "block") {
        	brandBar.css("display", "none");
        }
    });
});

// NEXT
window.onresize = function(){
	var nav = document.querySelector("nav ul");
	var mql = window.matchMedia("screen and (min-width: 500px)");

	if(mql.matches){
		nav.removeAttribute("style");
	}

	if (helper.checkForPull()){
		// console.log(document.getElementsByClassName("close"));
		if( document.getElementsByClassName("close").length === 0 ){
			// console.log("hello");
			var closes = [];
			for(var i = 0; i < 3; i++){
				var close = document.createElement("a");
				close.setAttribute("class", "fa fa-close fa-2x close");
				closes.push(close);
			}
			var parent = document.getElementById("brand-bar");
			// console.log(parent);
			parent.appendChild(closes[0]);
			var careers = document.getElementById("careers");
			careers.appendChild(closes[1]);
			var contact = document.getElementById("contact");
			contact.appendChild(closes[2]);
		}
	} else {
		if( document.getElementsByClassName("close").length !== 0 ){
			var close = document.getElementsByClassName("close");
			for( var c = 0; c < close.length; c++ ){
				var thisParent = close[c].parentNode;
				thisParent.removeChild(close[c]);
			}
		}
	}
	var close = document.getElementsByClassName("close");
	for( var c = 0; c < close.length; c++ ){
		var self = close[c];

		self.addEventListener("click", function(){
			event.preventDefault();
			var parent = this.parentNode;
			console.log(getComputedStyle(parent, null).display);
			if(getComputedStyle(parent, null).display == "block"){
				parent.style.display = "none";
			}
		});
	}
};

window.onload = function(){
	if (helper.checkForPull()){
		// console.log(document.getElementsByClassName("close"));
		if( document.getElementsByClassName("close").length == 0 ){
			console.log("hello")
			var closes = [];
			for(var i = 0; i < 3; i++){
				var close = document.createElement("a");
				close.setAttribute("class", "fa fa-close fa-2x close");
				closes.push(close);
			}
			var parent = document.getElementById("brand-bar");
			// console.log(parent);
			parent.appendChild(closes[0]);
			var careers = document.getElementById("careers");
			careers.appendChild(closes[1]);
			var contact = document.getElementById("contact");
			contact.appendChild(closes[2]);
		}
	} else {
		if( document.getElementsByClassName("close").length != 0 ){
			var close = document.getElementsByClassName("close");
			for( var c = 0; c < close.length; c++ ){
				var thisParent = close[c].parentNode;
				thisParent.removeChild(close[c]);
			}
		}
	}
	var close = document.getElementsByClassName("close");
	for( var c = 0; c < close.length; c++ ){
		var self = close[c];

		self.addEventListener("click", function(){
			event.preventDefault();
			var parent = this.parentNode;
			console.log(getComputedStyle(parent, null).display);
			if(getComputedStyle(parent, null).display == "block"){
				parent.style.display = "none";
			}
		});
	}


};
var helper = {
	checkForPull : function(){
		var pull = document.getElementById('pull');
		var pullDisplay = getComputedStyle(pull,null).display;
		var bool;
		if( pullDisplay == "block" ){
			return true;
		} else {
			return false;
		}
	},
	sendForm : function(formAction, formFullName, formEmail, formMessage, url){
		var action = formAction.value;
		var fullName = formFullName.value;
		var email = formEmail.value;
		var message = formMessage.value;
		var sendVal = "action="+action+"&fullname="+fullName+"&email="+email+"&message="+message;
		return new Promise(function (resolve, reject) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url);
			xhr.onload = function () {
				console.log("On Load");
			  if (this.status >= 200 && this.status < 300) {
			  	console.log("On Load resolve");
			    resolve(xhr.response);
			  } else {
			  	console.log("On Load Reject");
			    reject({
			      status: this.status,
			      statusText: xhr.statusText
			    });
			  }
			};
			xhr.onerror = function () {
				console.log("On Error");
			  reject({
			    status: this.status,
			    statusText: xhr.statusText
			  });
			};
			xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhr.send(sendVal);
		});
	}
};
// SMOOTH SCROLLING

$(document).ready(function () {
	    $(document).on("scroll", onScroll);

	    //smoothscroll
	    $('a[href^="#"]').on('click', function (e) {
	        e.preventDefault();
	        $(document).off("scroll");

	        $('a').each(function () {
	            $(this).removeClass('active');
	        });
	        $(this).addClass('active');

	        var target = this.hash,
	            menu = target;
	        $target = $(target);
	        $('html, body').stop().animate({
	            'scrollTop': $target.offset().top-140
	        }, 500, 'swing', function () {
	            window.location.hash = target;
	            $(document).on("scroll", onScroll);
	        });
	    });
	});

	function onScroll(event){
	    var scrollPos = $(document).scrollTop();
	    $('#menu-center a').each(function () {
	        var currLink = $(this);
	        var refElement = $(currLink.attr("href"));
	        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
	            $('#menu-center ul li a').removeClass("active");
	            currLink.addClass("active");
	        }
	        else{
	            currLink.removeClass("active");
	        }
	    });
	}
