// var $animation_elements = $('.myobp-img');
var anim_elements = document.getElementsByClassName('myobp-img');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);
  console.log(anim_elements)
  jQuery.each(anim_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    // if ((element_bottom_position >= window_top_position) &&
    //   (element_top_position <= window_bottom_position)) {
    // if (((element_top_position >= window_top_position) && (element_top_position < window_bottom_position)) ||
    //     ((element_top_position < window_top_position) && (element_bottom_position < window_bottom_position))  ){
    //   $element.addClass('fadeInUp');
    // } else {
    //   $element.removeClass('fadeInUp');
    // }
    if ((element_bottom_position >= window_top_position) &&
      (element_top_position <= window_bottom_position)) {
      $element.addClass('animated fadeInUp');
    } else {
      $element.removeClass('animated fadeInUp');
    }
  });
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');
