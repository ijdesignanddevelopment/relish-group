<?php
  // require_once('recaptchalib.php');
  // $privatekey = "6LfjbfkSAAAAAGCt8vdzHWIHEOY5amq5K6Kyrrjs";
  // $resp = recaptcha_check_answer ($privatekey,
  //                               $_SERVER["REMOTE_ADDR"],
  //                               $_POST["recaptcha_challenge_field"],
  //                               $_POST["recaptcha_response_field"]);

  // if (!$resp->is_valid) {
  //   // What happens when the CAPTCHA was entered incorrectly
  //   die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
  //        "(reCAPTCHA said: " . $resp->error . ")");
  // } else {
    // Your code here to handle a successful verification
    
      $fullname = htmlspecialchars($_POST['fullname']);
      $company = htmlspecialchars($_POST['company']);
      $mobile_num = htmlspecialchars($_POST['mobile_num']);
      $landline = htmlspecialchars($_POST['landline']);
      $email = htmlspecialchars($_POST['email']);
      $website = htmlspecialchars($_POST['website']);

      $concept = html_entity_decode($_POST['concept']);
      $description = htmlspecialchars($_POST['business_desc']);
      $location = htmlspecialchars($_POST['proposed_location']);
      $message = htmlspecialchars($_POST['message']);
      
      $formtype = "";
      $body = "You have received a new message from your website, relish-group.com: \n" .
            "From: " . $fullname . "\n" .
            "Email: " . $email . "\n";

      if ($_POST['action'] == "Apply" || $_POST['action'] == "Submit") {
        $body = $body . "\n" .
          "Message: " . $message;
        if ($_POST['action'] == "Apply") {
          $formtype = "Application";
        } else if ($_POST['action'] == "Submit") {
          $formtype = "Contact";
        } else {
          $formtype = "???";
        }
      } else {
        // if franchise
        $formtype = "Franchising";
        $body = $body . 
            "Company: " . $company . "\n" .
            "Mobile Number: " . $mobile_num . "\n" .
            "Landline: " . $landline . "\n" .
            "Website: " . $website . "\n\n" .
            "Concept Interested In: " . $concept . "\n" .
            "Business Description: " . $description . "\n" .
            "Proposed Location: " . $location;
      }

    // if (!empty($_POST['career-submit'])) {
	    mail("marketing@relish-group.com", "Relish Group - New " . $formtype . " Form Submission", $body);
	    header ("Location: thanks.php");

    // }
    
  // }
    
?>