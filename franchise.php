<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Welcome to Relish Group</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400italic,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,900' rel='stylesheet' type='text/css'>

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/relish.css">

  <!-- JS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <script src="js/jquery-2.1.4.min.js"></script>
  <script src="js/bootstrap.js"></script>

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  	<nav class="nav-collapse" style="z-index:1001;">
  		<div id="menu-center" class="limit">
  			<a href="http://www.relish-group.com/"><img id="relish-logo" src="images/relish-logo.png" /></a>
  			<a href="#" id="pull" class="fa fa-bars fa-2x"></a>
			<ul class="navi__list">
			    <li class="navi__listItem"><a href="index.php#about">About</a></li>
			    <li class="navi__listItem"><a class="brandsOn">Our Brands</a></li>
				<li class="navi__listItem"><a id="careersPull">Work With Us</a></li>
			    <li class="navi__listItem"><a id="contactPull">Contact Us</a></li>
          <li class="navi__listItem"><a href="franchise.php">Franchising</a></li>
			</ul>
		</div>
		<div class="popover" id="brand-bar" style="margin-top: 17px;">
			<div class="limit">
				<div class="container brand__row">
					<div class="two columns brand__item">
						<a href="/kumori"><img class="brand__logo" src="images/nav/brands-kumori-logo.png" width="100%"/></a>
					</div>
					<div class="two columns brand__item">
						<a href="/osakaohsho"><img class="brand__logo" src="osakaohsho/images/osaka-logo.png" width="100%"/></a>
					</div>
					<div class="two columns brand__item">
						<a href="/weenamkee"><img class="brand__logo brands-wnk-logo"src="images/nav/brands-wnk-logo.png"/></a>
					</div>
					<div class="two columns brand__item">
						<a href="/birdhouse"><img class="brand__logo"src="birdhouse/images/birdhouse-logo.png"/></a>
					</div>
					<div class="two columns brand__item">
						<a href="/rockseoul"><img class="brand__logo"src="rockseoul/images/rockseoul-logo-square.png"/></a>
					</div>
					<div class="two columns brand__item">
						<a href="/seoulstation"><img class="brand__logo"src="seoulstation/images/seoulstation-logo.png"/></a>
					</div>
					<!-- <div class="two columns brand__item">
						<a href="#"><img class="brand__logo" src="images/relish-logo.png" width="100%"/></a>
					</div>
					<div class="two columns brand__item">
						<a href="#"><img class="brand__logo" src="images/relish-logo.png" width="100%"/></a>
					</div>
					<div class="two columns brand__item">
						<a href="#"><img class="brand__logo" src="images/relish-logo.png" width="100%"/></a>
					</div> -->
				</div>
			</div>
		</div>
		<div class="popover" id="careers" style="margin-top: 32px;height:auto;padding-top: 25px;">
      <form action="mailhandler.php" method="post" id="careerForm" name="careerForm">
        <div class="limit">
          <div class="row">
            <div class="ten columns">
              <h1 class="popover-title">WORK WITH US</h1>
            </div>
            <div class="two columns">
              <input type="submit" name="submit" id="career-submit" value="Apply" />
            </div>
          </div>
          <div class="row">
            <div class="five columns">
              <input type="text" id="fullname" name="fullname" placeholder="Full Name" style="width: 100%;" form="careerForm" required><br>
              <input type="email" id="email" name="email" placeholder="Email Address" style="width: 100%;" form="careerForm" required>
            </div>
            <div class="seven columns">
              <textarea id="message" name="message" placeholder="Your Message" style="width: 100%; height: 100%;" form="careerForm" required></textarea>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="popover" id="contact" style="margin-top:32px;height:auto;padding-bottom:0px;padding-top: 25px;">
      <form method="post" action="mailhandler.php" id="contactForm" name="contactForm">
        <div class="limit">
          <div class="row">
            <div class="ten columns">
              <h1 class="popover-title">CONTACT US</h1>
            </div>
            <div class="two columns">
              <input type="submit" name="submit" id="contact-submit" value="Submit" />
            </div>
          </div>
          <div class="row">
            <div class="five columns">
              <input type="text" id="fullname" name="fullname" placeholder="Full Name" style="width: 100%;" form="contactForm" required><br>
              <input type="email" id="email" name="email" placeholder="Email Address" style="width: 100%;" form="contactForm" required>
            </div>
            <div class="seven columns">
              <textarea id="message" name="message" placeholder="Your Message" style="width: 100%; height: 100%;" form="contactForm" required></textarea>
            </div>
          </div>
        </div>
      </form>
    </div>
	</nav>

<div style="display:block; height:auto; position:relative; padding-top: 200px" class="carousel__container">
  <form method="post" action="mailhandler.php" id="franchisingForm">
    <div class="limit">
      <div class="row">
        <div class="ten columns">
          <h1 class="popover-title">FRANCHISE</h1>
        </div>
      </div>
      <div class="row">
        <div class="five columns">
          <input type="text" id="fullname" name="fullname" placeholder="Full Name" form="franchisingForm" required>
          <input type="text" id="company" name="company" placeholder="Company" form="franchisingForm">
          <input type="text" id="mobile_num" name="mobile_num" placeholder="Mobile Number" form="franchisingForm">
          <input type="text" id="landline" name="landline" placeholder="Landline Number" form="franchisingForm">
          <input type="email" id="email" name="email" placeholder="E-mail Address" form="franchisingForm" required>
          <input type="text" id="website" name="website" placeholder="Company Website" form="franchisingForm">
        </div>
        <div class="seven columns">
          <label>Concept interested in:
          <select name="concept" id="concept" form="franchisingForm" style="margin-left: 4px;" required>
            <option value="Osaka Ohsho">Osaka Ohsho</option>
            <option value="Wee Nam Kee">Wee Nam Kee</option>
            <option value="Kumori">Kumori</option>
            <option value="Bird House">Bird House</option>
            <option value="Rock &amp; Seoul">Rock &amp; Seoul</option>
          </select></label>
          <textarea id="business_desc" name="business_desc" placeholder="Short description of existing business" style="width: 100%; height: 100%;" rows="5"></textarea>
          <textarea id="proposed_location" name="proposed_location" placeholder="Proposed Location (if applicable)" style="width: 100%; height: 100%;"></textarea>
          <input type="submit" value="Submit">
        </div>
      </div>
    </div>
  </form>
</div>

<footer>
	<div class="row limit">
		<div class="three columns">
			<img class="footer-details" src="images/relish-footer-logo.jpg" />
		</div>
		<div class="nine columns footer-details">
			<div class="row">
				<p class="brand-promise">The purveyors of international hospitality casual dining concepts and brainchild of unique local culinary destinations.</p>
			</div>
			<div class="row">
				<div class="five columns">
					<p class="caption">Office Address</p>
					<p class="detail">2486 Taft Avenue, Pasay City 1300</p>
				</div>
				<div class="three columns">
					<p class="caption">Contact Number</p>
					<p class="detail">(+63) 02 552 7536</p>
				</div>
				<div class="four columns">
					<p class="caption">Email Address</p>
					<p class="detail">marketing@relish-group.com</p>
				</div>
			</div>
		</div>
	</div>
</footer>

<script src="js/relish.js" charset="utf-8"></script>
</body>
</html>
